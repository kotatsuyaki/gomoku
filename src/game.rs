use std::fmt::{Debug, Write};
use std::ops::RangeInclusive;

use imageproc::rect::Rect;
use ndarray::prelude::*;
use serde::{Deserialize, Serialize};

#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Stone {
    Black,
    White,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Board {
    data: Array2<Option<Stone>>,
    winner: Option<Stone>,
    size: usize,
    next: Stone,
    age: usize,
}

impl Debug for Board {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if f.alternate() {
            let mut text = String::new();
            writeln!(&mut text, "{}", "=".repeat(self.size + 2))?;
            for row in self.data.rows() {
                write!(&mut text, "|")?;
                for side in row.iter() {
                    write!(
                        &mut text,
                        "{}",
                        match side {
                            Some(Stone::Black) => '#',
                            Some(Stone::White) => 'o',
                            None => ' ',
                        }
                    )?;
                }
                write!(&mut text, "|\n")?;
            }
            writeln!(&mut text, "{}", "=".repeat(self.size + 2))?;

            f.debug_struct("Board")
                .field("winner", &self.winner)
                .field("size", &self.size)
                .field("next", &self.next)
                .field("age", &self.age)
                .field("board", &format_args!("\n{}", text))
                .finish()
        } else {
            f.debug_struct("Board")
                .field("winner", &self.winner)
                .field("size", &self.size)
                .field("next", &self.next)
                .field("age", &self.age)
                .finish()
        }
    }
}

#[allow(dead_code)]
impl Board {
    // read-only getters
    pub fn winner(&self) -> Option<Stone> {
        self.winner
    }

    pub fn age(&self) -> usize {
        self.age
    }

    pub fn is_ended(&self) -> bool {
        self.age == self.size * self.size || self.winner.is_some()
    }

    pub fn next(&self) -> Stone {
        self.next
    }

    pub fn size(&self) -> usize {
        self.size
    }

    pub fn is_empty_at(&self, (i, j): (usize, usize)) -> bool {
        self.data[(i, j)].is_none()
    }

    /// Create a new board that's empty
    pub fn new(size: usize) -> Self {
        let data = Array2::default((size, size));
        Self {
            data,
            size,
            winner: None,
            next: Stone::Black,
            age: 0,
        }
    }

    #[allow(dead_code)]
    pub fn print(&self) {
        println!("{}", "=".repeat(self.size + 2));
        for row in self.data.rows() {
            print!("|");
            for side in row.iter() {
                print!(
                    "{}",
                    match side {
                        Some(Stone::Black) => '#',
                        Some(Stone::White) => 'o',
                        None => ' ',
                    }
                )
            }
            print!("|\n");
        }
        println!("{}", "=".repeat(self.size + 2));
    }

    /// Iterators to the four directions, 2 of them for vertical (i) and 2 of them for horizontal (j).
    /// The order is (up, down, left, right).
    ///
    /// To illustrate, consider this situation.
    ///
    /// ```
    /// +--------
    /// |  .
    /// |..x.....
    /// |  .
    /// |  .
    /// |  .
    /// |  .
    /// |  .
    /// ```
    ///
    /// If x is the position (i, j), then we have
    /// * upit = [0]
    /// * downit = [2, 3, 4, 5, 6]
    /// * leftit = [1, 0]
    /// * rightit = [3, 4, 5, 6, 7]
    fn dir_iters(
        max_idx: usize,
        (i, j): (usize, usize),
    ) -> (
        std::iter::Rev<std::ops::Range<usize>>,
        RangeInclusive<usize>,
        std::iter::Rev<std::ops::Range<usize>>,
        RangeInclusive<usize>,
    ) {
        let upit = (i.saturating_sub(5)..i).rev();
        let downit = (i + 1)..=(i + 5).min(max_idx);
        let leftit = (j.saturating_sub(5)..j).rev();
        let rightit = (j + 1)..=(j + 5).min(max_idx);
        (upit, downit, leftit, rightit)
    }

    /// Non-panicing `place()`
    pub fn try_place(&mut self, (i, j): (usize, usize)) -> Result<(), Error> {
        let max_idx = self.size - 1;
        if i > max_idx || j > max_idx {
            return Err(Error::IndexOob(i, j));
        }
        if self.data[(i, j)].is_some() {
            return Err(Error::IndexOccupied(i, j));
        }

        self.place((i, j));

        Ok(())
    }

    /// Place a stone at (i, j).  It's the caller's reponsibility to check if:
    /// 1. The coordinates is within bounds
    /// 2. The place is empty
    ///
    /// Invalid usage leads to a panic.
    pub fn place(&mut self, (i, j): (usize, usize)) {
        let max_idx = self.size - 1;

        if i > max_idx || j > max_idx {
            panic!("(i, j) oob");
        }

        if self.data[(i, j)].is_some() {
            panic!("(i, j) occupied");
        }

        self.data[(i, j)] = Some(self.next);
        let me = self.next;
        let opponent = match self.next {
            Stone::Black => Stone::White,
            Stone::White => Stone::Black,
        };

        let (upit, downit, leftit, rightit) = Self::dir_iters(max_idx, (i, j));

        // horizontal
        let mut count = 1;
        for j in leftit.clone() {
            if self.data[(i, j)] == Some(me) {
                count += 1;
            } else {
                break;
            }
        }
        for j in rightit.clone() {
            if self.data[(i, j)] == Some(me) {
                count += 1;
            } else {
                break;
            }
        }

        if count == 5 {
            self.winner = Some(me);
        }

        // vertical
        let mut count = 1;
        for i in downit.clone() {
            if self.data[(i, j)] == Some(me) {
                count += 1;
            } else {
                break;
            }
        }

        for i in upit.clone() {
            if self.data[(i, j)] == Some(me) {
                count += 1;
            } else {
                break;
            }
        }

        if count == 5 {
            self.winner = Some(me)
        }

        // diagonal
        let mut count = 1;
        for (i, j) in upit.clone().zip(leftit.clone()) {
            if self.data[(i, j)] == Some(me) {
                count += 1;
            } else {
                break;
            }
        }

        for (i, j) in downit.clone().zip(rightit.clone()) {
            if self.data[(i, j)] == Some(me) {
                count += 1;
            } else {
                break;
            }
        }

        if count == 5 {
            self.winner = Some(me)
        }

        // secondary diagonal
        let mut count = 1;
        for (i, j) in upit.zip(rightit) {
            if self.data[(i, j)] == Some(me) {
                count += 1;
            } else {
                break;
            }
        }

        for (i, j) in downit.zip(leftit) {
            if self.data[(i, j)] == Some(me) {
                count += 1;
            } else {
                break;
            }
        }

        if count == 5 {
            self.winner = Some(me)
        }

        self.next = opponent;
        self.age += 1;
    }

    pub fn get_empty_places(&self) -> impl Iterator<Item = (usize, usize)> + '_ {
        self.data.indexed_iter().filter_map(
            |(place, stone)| {
                if stone.is_none() {
                    Some(place)
                } else {
                    None
                }
            },
        )
    }

    pub fn image(&self) -> image::RgbaImage {
        use image::{imageops::overlay, ImageBuffer, Rgba};
        use imageproc::drawing::*;
        use rusttype::{Font, Scale};

        // config variables
        let cell_width = 64;
        let white_color = Rgba([242, 242, 242, 255]);
        let board_bg_color = Rgba([233, 171, 104, 255]);
        let white_img = image::load_from_memory(include_bytes!("../assets/white-64.png")).unwrap();
        let black_img = image::load_from_memory(include_bytes!("../assets/black-64.png")).unwrap();
        let font_bytes = include_bytes!("../assets/Fira_Mono/FiraMono-Regular.ttf");
        let font: Font<'static> = Font::try_from_bytes(font_bytes).unwrap();

        let side_len = (cell_width * self.size + cell_width / 2) as u32;

        let mut img = ImageBuffer::from_pixel(side_len, side_len, white_color);
        draw_filled_rect_mut(
            &mut img,
            Rect::at(0, 0).of_size(
                (cell_width * self.size) as u32,
                (cell_width * self.size) as u32,
            ),
            board_bg_color,
        );
        for i in 0..self.size {
            for j in 0..self.size {
                if let Some(stone_img) = self.data[(i, j)].map(|stone| match stone {
                    Stone::Black => black_img.as_rgba8().unwrap(),
                    Stone::White => white_img.as_rgba8().unwrap(),
                }) {
                    overlay(
                        &mut img,
                        stone_img,
                        j as u32 * cell_width as u32,
                        i as u32 * cell_width as u32,
                    );
                }
            }
            draw_text_mut(
                &mut img,
                Rgba([10, 10, 10, 255]),
                side_len - cell_width as u32 / 2 + 8,
                i as u32 * 64 + 16,
                Scale::uniform(24.0),
                &font,
                &format!("{}", char::from_digit(i as u32, 36).unwrap()),
            );
            let j = i;
            draw_text_mut(
                &mut img,
                Rgba([10, 10, 10, 255]),
                j as u32 * 64 + 16,
                side_len - cell_width as u32 / 2 + 8,
                Scale::uniform(24.0),
                &font,
                &format!("{}", char::from_digit(j as u32, 36).unwrap()),
            );
        }

        img
    }
}

#[allow(dead_code)]
pub(crate) fn rand_board(size: usize) -> Board {
    use rand::prelude::*;
    let mut board = Board::new(size);
    for i in 0..size {
        for j in 0..size {
            let rd = thread_rng().gen_range(0..size);
            if rd == 0 {
                board.data[(i, j)] = Some(Stone::Black);
                board.age += 1;
            } else if rd == 1 {
                board.data[(i, j)] = Some(Stone::White);
                board.age += 1;
            }
        }
    }
    board
}

#[allow(dead_code)]
pub(crate) fn ascii_to_board(text: &str) -> Board {
    let mut state = Board::new(5);
    for (i, line) in text.lines().enumerate() {
        for (j, ch) in line.chars().enumerate() {
            state.data[(i, j)] = match ch {
                '#' => Some(Stone::Black),
                'o' => Some(Stone::White),
                ' ' => None,
                _ => {
                    panic!()
                }
            };
            if state.data[(i, j)].is_some() {
                state.age += 1;
            }
        }
    }
    state
}

#[derive(Debug)]
pub enum Error {
    IndexOob(usize, usize),
    IndexOccupied(usize, usize),
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::IndexOob(i, j) => {
                write!(f, "({}, {}) is oob", i, j)
            }
            Error::IndexOccupied(i, j) => {
                write!(f, "({}, {}) is occupied", i, j)
            }
        }
    }
}

impl std::error::Error for Error {}

#[cfg(test)]
mod test {
    use super::*;

    // Ensure that they're small enough
    #[test]
    fn side_size() {
        assert_eq!(std::mem::size_of::<Stone>(), 1);
        assert_eq!(std::mem::size_of::<Option<Stone>>(), 1);
    }

    #[test]
    fn board_dir_iters() {
        let size = 15;
        let (a, b, c, d) = Board::dir_iters(size, (4, 3));
        assert_eq!(a.count(), 4);
        assert_eq!(b.count(), 5);
        assert_eq!(c.count(), 3);
        assert_eq!(d.count(), 5);

        let size = 19;
        let (a, b, c, d) = Board::dir_iters(size, (8, 8));
        assert_eq!(a.count(), 5);
        assert_eq!(b.count(), 5);
        assert_eq!(c.count(), 5);
        assert_eq!(d.count(), 5);

        let size = 15;
        let (a, b, c, d) = Board::dir_iters(size, (12, 13));
        assert_eq!(a.count(), 5);
        assert_eq!(b.count(), 3);
        assert_eq!(c.count(), 5);
        assert_eq!(d.count(), 2);
    }
}
